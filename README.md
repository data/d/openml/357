# OpenML dataset: vehicle_sensIT

https://www.openml.org/d/357

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: M. Duarte, Y. H. Hu  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets) - 2013-11-14 -   
**Please cite**:   M. Duarte and Y. H. Hu. 
Vehicle classification in distributed sensor networks. 
Journal of Parallel and Distributed Computing, 64(7):826-838, July 2004.


This is the SensIT Vehicle (combined)  dataset, retrieved 2013-11-14 from the libSVM site. Additional to the preprocessing done there (see LibSVM site for details), this dataset was created as follows: 
-join test and train datasets (2 files, already pre-combined)
-relabel classes 1,2=positive class and 3=negative class
-normalize each file columnwise according to the following rules: 
-If a column only contains one value (constant feature), it will set to zero and thus removed by sparsity. 
-If a column contains two values (binary feature), the value occuring more often will be set to zero, the other to one. -If a column contains more than two values (multinary/real feature), the column is divided by its std deviation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/357) of an [OpenML dataset](https://www.openml.org/d/357). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/357/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/357/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/357/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

